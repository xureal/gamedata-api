var gameSessionID;
var gameAppName;
var gameLogs = true;
var gameTimerInterval;
var gameData = {
    details: {
        "score": 0,
        "multiplier": 1,
        "timer": 0,
        "timerLimit": 300,
        "balance": 50,
        "hiscore": 0,
        "playerHP": 100,
        "playerID": "none",
        "playerName": "none"
    },
    creds: {
        type: "none"
    },
    callbackList: {
        "logging": "none"
    },
    init: function (credsObj) {
        gameAppName = credsObj.name;
        gameData._log("game - initiated");
        gameData._log("game - creds name: "+ credsObj.name);  
        gameData._log("game - creds type: "+ credsObj.type);
        gameData._log("game - creds title: "+ credsObj.title); 
        this._genSession();
        if (credsObj.type == "pf") {
            gameData._log("register pf creds");
            gameData.creds.type = "pf";
            pfConnection.init(credsObj);
        }
        gameData._log("setting message");
        window.addEventListener('message', function (e) {
            let event_data = e.data;
            if (event_data.type == 'game_event') {
                //gameData._log("received data", event_data);
            }
            if (event_data.type == 'game_event' && event_data.action == 'enemy_hit') {
                gameData.updateScore(event_data.value);
                gameData.updateMultiplier('add');
            } else if (event_data.type == 'game_event' && event_data.action == 'player_hit') {
                gameData.updateHP('subtract');
                gameData.updateMultiplier('reset');
            } else if (event_data.type == 'game_event' && event_data.action == 'player_heal') {
                gameData.updateHP('add');
            } else if (event_data.type == 'game_event' && event_data.action == 'game_start') {
                gameData.startTimer(gameData.details.timerLimit);
                gameData._log("game_start triggered");
            } else if (event_data.type == 'game_event' && event_data.action == 'game_end') {
                gameData.postScore();
            } else if (event_data.type == 'game_event' && event_data.action == 'game_reset') {
                gameData.resetData();
            }

            document.dispatchEvent(new CustomEvent('gameDataUpdates', {
                detail: {
                    "action": event_data.action
                }
            }));
        });
    },
    updateScore: function (num) {
        gameData.details.score += num * gameData.details.multiplier;
        gameData._log("score: " + gameData.details.score);
    },
    updateTotalScore: function () {
        gameData.details.hiscore += gameData.details.score;
        gameData._log("hiscore: " + gameData.details.hiscore);
    },
    updateMultiplier: function (type) {
        if (type == "add") {
            gameData.details.multiplier++;
        } else if (type == "reset") {
            gameData.details.multiplier = 1;
        }
        gameData._log("multiplier: " + gameData.details.multiplier);
    },
    updateHP: function (type) {
        var damage = 10;
        var heal = 5;
        if (type == "add") {
            gameData.details.playerHP += heal;
            if (gameData.details.playerHP > 100) {
                gameData.details.playerHP = 100;
                gameData._log("player full health");
            }
        } else if (type == "subtract") {
            gameData.details.playerHP -= damage;
            if (gameData.details.playerHP < 0) {
                gameData.details.playerHP = 0;
                gameData._log("player killed");
            }
        }
        gameData._log("player: " + gameData.details.playerHP);
    },
    startTimer: function (limit) {
        gameTimerInterval = setInterval(function () {
            gameData.details.timer += 1;
            var currentTimeDuration = gameData.formatTimerMS(gameData.details.timer);
            if (gameData.details.timer > limit) {
                clearInterval(gameTimerInterval);
                gameData.details.timer = limit;
            } else {
                document.dispatchEvent(new CustomEvent('gameDataTimerUpdate', {
                    detail: {
                        "timeFormat": "0" + currentTimeDuration,
                        "timerLimit": gameData.details.timerLimit,
                        "timer": gameData.details.timer
                    }
                }));

            }
        }, 1000);

    },
    formatTimerMS: function (s) {
        return (s - (s %= 60)) / 60 + (9 < s ? ':' : ':0') + s
    },
    logPlayer: function (_player,_callback) {
        var playerName = _player;
        gameData._log(playerName);
        if (playerName) {
            gameData.details.playerName = playerName;
            gameData.callbackList.logging = _callback;
            console.log("for pf connection login: "+playerName+" callback:", gameData.logPlayerSuccess);
            pfConnection.initPlayer(playerName, gameData.logPlayerSuccess);
        } else {
            alert("PLAYER NAME REQUIRED");
        }
    },
    logPlayerSuccess: function (msg, pid) {
        if (msg == "SUCCESS") {
            gameData._log("------------------- user logged");
            gameData._log("------------------- user id: ", pid);
            gameData.details.playerID = pid;
            var callbackFunction = gameData.callbackList.logging;
            var playerObject = {
                "name": gameData.details.playerName,
                "success": true 
            }
            callbackFunction(playerObject);
        } else {
            gameData._log("error:", msg);
            var playerObject = {
                "success": false 
            }
            callbackFunction(playerObject);
        }
    },
    getAll: function () {
        return gameData.details;
    },
    resetData: function () {
        clearInterval(gameTimerInterval);
        gameData.details.score = 0;
        gameData.details.multiplier = 1;
        gameData.details.timer = 0;
        gameData.details.balance = 50;
        gameData.details.hiscore = 0;
        gameData.details.playerHP = 100;
        return gameData.details;
    },
    postScore: function () {
        var gameScore = gameData.details.score;
        pfConnection.transferScore(gameScore, gameData.postScoreSuccess);
    },
    postScoreSuccess: function (msg) {
        if (msg == "SUCCESS") {
            gameData._log("------------------- score posted: " + gameData.details.score);
        } else {
            gameData._log("error:", msg);
        }
    },
    getLeaderboard: function() {
        pfConnection.getScoreBoard(gameData.getLeaderboardSuccess);
    },
    getLeaderboardSuccess: function(data) {
         console.log("leaderboard: ", data);
    },
    getLeaderboardData: function(_callback) {
        var leadearboardData = function(data) {
            _callback(data.Leaderboard)
        };
        pfConnection.getScoreBoard(leadearboardData);
    },
    getHighscoreData: function(_callback) {
        var leadearboardData = function(data) {
            for (let i = 0; i < data.Leaderboard.length; i++) {
                if (gameData.details.playerID  ==  data.Leaderboard[i].PlayFabId) {
                    console.log(data.Leaderboard[i].PlayFabId);
                    gameData.details.hiscore = data.Leaderboard[i].StatValue;
                     _callback(gameData.details.hiscore);
                    return;
                }
            }
             _callback(0);
           
        };
        pfConnection.getScoreBoard(leadearboardData);
    },
    appEvent: function (stateType) {
        var _this = this;
        this._genSession();
        var urlParams = new URLSearchParams(window.location.search);
        var _appname = gameAppName;
        var appDataEvent = {
            app: _appname,
            action: gameAppName + "-" + stateType,
            session: gameSessionID
        };
        gameData._log("App Data Log: ", appDataEvent);
        window.top.postMessage(appDataEvent, '*');
    },
    _genSession: function () {
        var sNum = Math.random();
        sNum.toString(36);
        var sID = sNum.toString(36).substr(2, 9);
        sID.length >= 9;
        gameData._log("gameSessionID: " + sID);
        gameSessionID = sID;
    },
    _log: function (lval) {
        if (gameLogs) {
            console.log(lval);
        }
    }
};